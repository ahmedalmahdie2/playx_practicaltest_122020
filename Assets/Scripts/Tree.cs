﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mahdie.playx.parcticalTest2020
{
	public interface ITree
	{
		int TrunksNum { get; set; }
		int TrunkHits { get; set; }
		List<Trunk> MyTrunks { get; set; }
		bool HasBeenDestroyed();
	}

	/**
	* ************ Tree *************
	* Author: Ahmed Almahdie
	* Date: 121620
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	* stores a Tree's data in runtime
	* ****************************************/

	[System.Serializable]
	public class Tree : ITree
	{
		#region properties, variables, and declerations
		// the tree's gameObject
		public GameObject TreeObject;

		// the defined trunks number in this tree
		public int TrunksNum { get; set; }

		// stores the trunk hits from the TreeSettings scriptableObject
		public int TrunkHits { get; set; }
		// list of all Trunk instances in this tree
		public List<Trunk> MyTrunks { get; set; }
		#endregion

		#region Helper methods


		#endregion

		#region public methods
		public Tree(int trunksNum, int trunkChops, GameObject treeObject = null)
		{
			TrunksNum = trunksNum;
			TrunkHits = trunkChops;
			TreeObject = treeObject;
			MyTrunks = new List<Trunk>();
		}

		/// <summary>
		/// checking if all of the tree's trunks have been destroyed
		/// </summary>
		/// <returns></returns>
		public bool HasBeenDestroyed()
		{
			return MyTrunks.TrueForAll(x => !x.CanGetHit);
		}
		#endregion
	}

}
