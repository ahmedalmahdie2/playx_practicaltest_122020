﻿using UnityEngine;

namespace mahdie
{
	public interface IShooterSettings
	{
	
	}
    /**
	* ************ ShooterSettings *************
	* Author: Ahmed Almahdie
	* Date: 121820
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	*
	* ****************************************/
	[CreateAssetMenu(menuName = "Single Instances/New Shooter Settings")]
	public class ShooterSettings : ScriptableObject
	{
		#region properties and dependencies
		[SerializeField] [Range (0, 1)]float _shootingRate = 0.1f;
		public float ShootingRate => _shootingRate;
		[SerializeField] [Range(1, 2)] float _projectileSpeedFactor = 1;
		public float ProjectileSpeedFactor => _projectileSpeedFactor;

		#endregion
	}

}
