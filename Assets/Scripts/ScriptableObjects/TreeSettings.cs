﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mahdie.playx.parcticalTest2020
{
	/**
	* ************ TreeSettings *************
	* Author: Ahmed Almahdie
	* Date: 121620
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	* Defines the tree's prefab, min and max trunks count
	* ****************************************/

	[CreateAssetMenu(menuName = "Single Instances/New Tree Settings")]
	public class TreeSettings : ScriptableObject
	{
		#region properties, variables, and declerations
		[SerializeField] private GameObject _treePrefab;
		public GameObject TreePrefab => _treePrefab;

		[SerializeField] private int _minTrunksNum;
		public int MinTrunksNum => _minTrunksNum;

		[SerializeField] private int _maxTrunksNum;
		public int MaxTrunksNum => _maxTrunksNum;

		// how many chops needed to destroy a single Trunk.
		[SerializeField] private int _trunkHits;
		public int TrunkHits => _trunkHits;
		#endregion
	}

}
