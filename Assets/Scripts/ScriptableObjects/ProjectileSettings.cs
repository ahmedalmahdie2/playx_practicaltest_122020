﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mahdie.playx.parcticalTest2020
{
	/**
	* ************ ProjectileSettings *************
	* Author: Ahmed Almahdie
	* Date: 121520
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	*
	* ****************************************/
	[CreateAssetMenu(menuName = "Single Instances/New Projectile Settings")]
	public class ProjectileSettings : ScriptableObject
	{
		#region properties and dependencies
		[SerializeField] GameObject _projectilePrefab;
		public GameObject ProjectilePrefab => _projectilePrefab;
		[SerializeField] DamageSettings _damageSettings;
		public DamageSettings MyDamageSettings => _damageSettings;
		[SerializeField] float _speed;
		public float Speed => _speed;
		#endregion
	}
	
}
