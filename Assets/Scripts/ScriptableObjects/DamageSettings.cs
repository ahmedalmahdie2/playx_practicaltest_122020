﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mahdie.playx.parcticalTest2020
{
    /**
	* ************ DamageSettings *************
	* Author: Ahmed Almahdie
	* Date: 121520
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	*
	* ****************************************/

	[CreateAssetMenu(menuName = "Single Instances/New Damage Settings")]
	public class DamageSettings : ScriptableObject
	{
		#region properties and dependencies
		[SerializeField] Damage _damageValue;
		public Damage DamageValue => _damageValue;
		#endregion
	}
	
}
