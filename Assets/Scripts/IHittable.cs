﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace mahdie.playx.parcticalTest2020
{
	/**
	* ************ IHittable *************
	* Author: Ahmed Almahdie
	* Date: 121520
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	* It marks any hittable object.
	* ****************************************/

	public interface IHittable
	{
		int MaxHits { get; set; }
		int RemainingHits { get; set; }
		bool CanGetHit { get; }
		void GetHit(Damage damage);
	}

	/**
	* ************ Damage *************
	* Author: Ahmed Almahdie
	* Date: 121520
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	* It represents the Damage value that some
	* Objects might have.
	* ****************************************/

	[System.Serializable]
	public class Damage
	{
		[SerializeField] int _value;
		public int Value { get { return _value; } set { _value = value; } }
		public bool IsConsumed { get; set; }

		public Damage()
		{
			SetDamage(1);
			IsConsumed = false;
		}

		public Damage(int damage)
		{
			SetDamage(damage);
			IsConsumed = false;
		}

		public void ConsumeDamage()
		{
			Value = 0;
			IsConsumed = true;
		}

		public void SetDamage(int damage)
		{
			Value = damage;
		}
	}

}
