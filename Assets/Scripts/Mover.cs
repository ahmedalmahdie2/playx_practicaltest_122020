﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace mahdie.playx.parcticalTest2020
{
	public interface IMover
	{
		Transform Root { get; }
		float Speed { get; set; }
		bool CanMove { get; set; }
		bool IsMoving { get; }
		Vector3 TargetPosition { get; set; }
		void MoveTo(Vector3 target);
		void Stop();
		void SetPosition(Vector3 position);
	}
	/**
	* ************ Mover *************
	* Author: Ahmed Almahdie
	* Date: 121520
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	*
	* ****************************************/
	public class Mover : MonoBehaviour, IMover
	{
		#region properties and dependencies
		[SerializeField] Transform _root;
		public Transform Root => _root;

		[SerializeField] float _speed;
		public float Speed { get { return _speed; } set { _speed = value; } }

		public bool CanMove { get; set; }
		public bool IsMoving { get; protected set; }
		public Vector3 TargetPosition { get; set; }

		public UnityAction<Mover> OnReachedTarget;

		protected Vector3 _currentPosition;
		protected float _step; // distance to be taken in this frame.
		#endregion

		#region mono methods
		protected void Awake()
		{
			if (!enabled) return;
			if (!_root) _root = transform;
		}
		// Start is called before the first frame update
		protected void Start()
		{
			
		}
		#endregion

		#region helper methods

		#region Coroutines
		protected IEnumerator MoveCoroutine()
		{
			var dist = Vector3.Distance(_root.position, TargetPosition);
			//Debug.Log(name + " distance to target: " + dist);

			_step = Time.deltaTime * _speed;
			while (dist > _step && IsMoving)
			{

				dist = Vector3.Distance(_root.position, TargetPosition);
				_step = Time.deltaTime * _speed;

				_root.position = Vector3.MoveTowards(_root.position, TargetPosition, _step);
				yield return null;
			}

			if (IsMoving) _root.position = TargetPosition;

			IsMoving = false;

			OnReachedTarget?.Invoke(this);
		}
		#endregion
		#endregion

		#region public methods
		public void SetPosition(Vector3 position)
		{
			if (IsMoving) return;
			_root.position = position;
		}

		public virtual void MoveTo(Vector3 targetPosition)
		{
			if (IsMoving) return;

			TargetPosition = targetPosition;
			IsMoving = true;
			StartCoroutine(MoveCoroutine());
		}

		public virtual void Move(Transform target)
		{
			if (!target) return;
			
			MoveTo(target.position);
		}

		public virtual void Stop()
		{
			if (!IsMoving) return;

			IsMoving = false;
		}
		#endregion
	}
	
}
