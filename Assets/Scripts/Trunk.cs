﻿using UnityEngine;
using UnityEngine.Events;

namespace mahdie.playx.parcticalTest2020
{
	public interface ITrunk : IHittable
	{
		Transform NextTrunkTransform { get; }
		TreeController MyTreeController { get; set; }
	}

	/**
	* ************ Trunk *************
	* Author: Ahmed Almahdie
	* Date: 121620
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	* controlls a single trunk in a tree
	* ****************************************/

	public class Trunk : MonoBehaviour, ITrunk
	{
		#region properties, variables, and declerations
		// marks the next trunk position
		[SerializeField] private Transform _nextTrunkTransform;
		public Transform NextTrunkTransform => _nextTrunkTransform;

		/// <summary>
		/// number of chops at which the trunk should be completely chopped.
		/// </summary>
		public int MaxHits { get; set; }

		/// <summary>
		/// remaining chops till the trunk is chopped.
		/// </summary>
		public int RemainingHits { get; set; }

		/// <summary>
		/// ref for the treeController of this trunk
		/// </summary>
		public TreeController MyTreeController { get; set; }

		/// <summary>
		/// checks if a trunk can get hit.
		/// </summary>
		public bool CanGetHit => RemainingHits > 0;

		// invoked when the trunk is hit
		public UnityAction<Trunk> OnTrunkHit;
		#endregion

		#region public methods
		/// <summary>
		/// initializing the trunk's data, max and remaining chops
		/// </summary>
		/// <param name="maxHits"></param>
		public void Initialize(int maxHits = 0)
		{
			MaxHits = maxHits > 0 ? maxHits : 1;
			RemainingHits = MaxHits;
		}

		/// <summary>
		/// reducing num of remaining chops by one (for now)
		/// </summary>
		public void GetHit(Damage damage)
		{
			if (RemainingHits - damage.Value < 0)
			{
				Debug.Log("trunk has no remainig hits!");
				RemainingHits = 0;
			}
			else
			{
				RemainingHits -= damage.Value;
				Debug.Log($"trunk has {RemainingHits} remainig hits!");
				OnTrunkHit?.Invoke(this);
			}
		}
		#endregion
	}
}
