﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mahdie.playx.parcticalTest2020
{
	public interface IShooter
	{
		bool CanShoot { get; set; }
		ShooterSettings MyShooterSettings { get; }
		ProjectileSettings MyProjectileSettings { get; set; }

		void Shoot();
	}
	/**
	* ************ Shooter *************
	* Author: Ahmed Almahdie
	* Date: 121820
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	* responsible for firing up the projectiles
	* ****************************************/
	public class Shooter : MonoBehaviour, IShooter
	{
		#region properties and dependencies
		// shooter settings
		[SerializeField] private ShooterSettings _myShooterSettings;
		public ShooterSettings MyShooterSettings => _myShooterSettings;

		// projectile settings
		[SerializeField] private ProjectileSettings _myProjectileSettings;
		public ProjectileSettings MyProjectileSettings
		{
			get { return _myProjectileSettings; }
			set { _myProjectileSettings = value; }
		}

		// This stores the onbject with the direction to shoot at projectiles.
		[SerializeField] private Transform _aim;

		// allows the Shooter to fire off a blade
		public bool CanShoot { get; set; }

		// just keeping the blade's RateOfshooting cached in this variable
		private float _shootingCoolDownTime;
		// to track the rate of shooting time
		private float _timer;
		#endregion

		#region mono Methods
		private void Start()
		{
			initialize();
		}

		private void Update()
		{
			// tracking the cool down time
			if (_timer < _shootingCoolDownTime)
			{
				_timer += Time.deltaTime;
			}
			else
			{
				//if(!CanChop)
				//	Debug.Log("Can Chop Now");

				CanShoot = true;
			}
		}
		#endregion

		#region Helper methods
		/// <summary>
		/// initializing the chopper's variables
		/// </summary>
		private void initialize()
		{
			// we only gonna use rateOfChopping from the settings object!
			_shootingCoolDownTime = _myShooterSettings.ShootingRate;
			_timer = 0;
			CanShoot = false;
		}
		#endregion

		#region public methods
		/// <summary>
		/// if the shooter canShoot, it will fire a projectile at the defined aim direction, and 
		/// if it hits an IHittable object, it will call its GetHit method.
		/// we don't control the range of the raycase for now, but we should!
		/// </summary>
		public void Shoot()
		{
			if (CanShoot)
			{

				Debug.DrawRay(_aim.position, _aim.forward * 1000, Color.red, 10);

				RaycastHit _hit;
				if (Physics.Raycast(_aim.position, _aim.forward * 1000, out _hit))
				{
					if (_hit.transform.GetComponentInParent<IHittable>() != null)
					{
						//Debug.Log("Hit: " + _hit.transform.name);
						_hit.transform.GetComponentInParent<IHittable>().GetHit(MyProjectileSettings.MyDamageSettings.DamageValue);
					}
				}

				// to force the cool down again
				initialize();
			}
		}
		#endregion
	}
	
}
