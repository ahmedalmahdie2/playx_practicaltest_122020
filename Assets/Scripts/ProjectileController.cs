﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mahdie.playx.parcticalTest2020
{
	public interface IProjectile
	{
		Damage MyDamage { get; set; }
	}
	/**
	* ************ ProjectileController *************
	* Author: Ahmed Almahdie
	* Date: 121520
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	* It Controls all the different aspects of the projectile
	* behaviour, from movement, damage, to animations and effects.
	* ****************************************/
	public class ProjectileController : MonoBehaviour, IProjectile
	{
		#region properties and dependencies

		[SerializeField] ProjectileSettings _projectileSettings;
		public Damage MyDamage { get; set; }

		#endregion

		#region mono methods
		// Start is called before the first frame update
		void Start()
		{
			
		}

		// Update is called once per frame
		void Update()
		{
			
		}
		#endregion

		#region helper methods
		
		#region Coroutines
		
		#endregion
		#endregion
		
		#region public methods
		#endregion
	}
	
}
