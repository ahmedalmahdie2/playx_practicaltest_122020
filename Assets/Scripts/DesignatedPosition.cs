﻿using UnityEngine;

namespace mahdie.playx.parcticalTest2020
{
	public interface IDesignatedPosition
	{
		Vector3 Position { get; }
		bool IsFree { get; set; }
	}

	/**
	* ************ DesignatedPosition *************
	* Author: Ahmed Almahdie
	* Date: 121620
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	* This will be used to mark a Transform in the Scene
	* as a special position.
	* ****************************************/

	public class DesignatedPosition : IDesignatedPosition
	{
		#region properties, variables, and declerations
		/// <summary>
		/// spot's position in scene
		/// </summary>
		public Vector3 Position { get; protected set; }

		/// <summary>
		/// if this spot was taken before. I doubt that I'd use it to its fullest potential!
		/// but it seems needed anyway.
		/// </summary>
		public bool IsFree { get; set; }
		#endregion

		#region Constructor
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="position">world position of the CustomPosition.</param>
		/// <param name="isFree">whether this CustomPosition is free to use or not.</param>
		public DesignatedPosition(Vector3 position, bool isFree = true)
		{
			Position = position;
			IsFree = isFree;
		}
		#endregion

		#region Helper methods

		#endregion

		#region public methods

		#endregion
	}

}
