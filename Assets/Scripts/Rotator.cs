﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace mahdie.playx.parcticalTest2020
{
	/**
	* ************ Rotator *************
	* Author: Ahmed Almahdie
	* Date: 121820
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	*
	* ****************************************/

	public class Rotator : MonoBehaviour
	{
		#region properties, variables, and declerations
		[SerializeField] bool _playOnAwake;
		public bool PlayOnAwake => _playOnAwake;

		[SerializeField] bool _isLocalSpace = false;
		public bool IsLocalSpace
		{
			get { return _isLocalSpace; }
			set { _isLocalSpace = value; }
		}

		[SerializeField] Vector3 _rotationAxis;
		public Vector3 RotationAxis
		{
			get { return _rotationAxis; }
			set { _rotationAxis = value; }
		}

		[SerializeField] Transform _rootTransform;
		public Transform RootTransform => _rootTransform;
		
		[SerializeField] float _timeToWaitToStop;
		public float TimeToWaitToStop
		{
			get { return _timeToWaitToStop; }
			set { _timeToWaitToStop = value; }
		}
		public Quaternion? StopRotation { get; set; }
		public bool IsRotating { get; protected set; }

		public UnityAction OnReachedTargetRotation;
		protected float _timer;
		#endregion

		#region mono methods
		protected void Awake()
		{
			if (!enabled) return;
			if (!_rootTransform) _rootTransform = transform;

			if(PlayOnAwake)
			{
				Rotate(_rotationAxis);
			}
		}

		protected void Start()
		{

		}

		protected void Update()
		{
			if(IsRotating)
			{
				if(TimeToWaitToStop < 0 || _timer < TimeToWaitToStop/* || IsSkippingToNextAction*/)
				{
					_rootTransform.Rotate(RotationAxis * Time.deltaTime, _isLocalSpace ? Space.Self : Space.World);

					if (TimeToWaitToStop > 0)
					{
						_timer += Time.deltaTime;
					}
					return;
				}

				IsRotating = false;
				_timer = 0;

				OnReachedTargetRotation?.Invoke();
			}
		}
		#endregion

		#region Helper methods

		#endregion

		#region public methods
		public virtual void Rotate(Vector3 rotationAxis, bool isSkippable = false, float forTime = -1)
		{
			RotationAxis = rotationAxis;
			TimeToWaitToStop = forTime;
			_timer = 0;
			IsRotating = true;
		}

		public virtual void StopRotating()
		{
			if (!IsRotating) return;
			IsRotating = false;
			_timer = 0;

			if (StopRotation.HasValue)
				_rootTransform.rotation = StopRotation.Value;
		}

		public virtual void SkipToNextAction()
		{

		}
		#endregion
	}
}
